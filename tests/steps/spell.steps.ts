import { Page } from "@playwright/test";
import { navigateToFavoritePage } from "../common/navigate";

export const getSpellSelector = (spellIndex?: string) => {
  if (!spellIndex) return `[data-testid^="spell-item"]`;

  return `[data-testid="spell-item-${spellIndex}"]`;
};

export const getSpellIndexspellIndexSelector = (spellIndex?: string) => {
  if (!spellIndex) return `[data-testid^="spell-item-name"]`;

  return `[data-testid="spell-item-name-${spellIndex}"]`;
};

export const getSpellFavoriteButtonSelector = (spellIndex?: string) => {
  if (!spellIndex) return `[data-testid^="spell-item-favorite-button"]`;

  return `[data-testid="spell-item-favorite-button-${spellIndex}"]`;
};

export const waitForSpells = async ({ page }: { page: Page }) => {
  const spellsLocator = page.locator(getSpellSelector());

  await spellsLocator.nth(0).waitFor({ timeout: 10000 });

  return spellsLocator;
};

export const waitForSpell = async ({ page, spellIndex }: { page: Page, spellIndex: string }) => {
  const spellLocator = page.locator(getSpellIndexspellIndexSelector(spellIndex));

  await spellLocator.waitFor({ timeout: 10000 });

  return spellLocator;
};

export const waitForFirstSpell = async ({ page }: { page: Page }) => {
  const spellsLocator = await waitForSpells({ page });

  return spellsLocator.nth(0);
};

export const addFirstSpellToFavorite = async ({ page }: { page: Page }) => {
  const spellLocator = await waitForFirstSpell({ page });
  const spellIndex = await spellLocator.getAttribute('id') as string;

  const addToFavoriteButton = spellLocator.locator(getSpellFavoriteButtonSelector(spellIndex));
  await addToFavoriteButton.click();
  await addToFavoriteButton.locator('[data-testid="FavoriteIcon"]').waitFor();

  return spellIndex;
};

export const checkSpellInFavoriteList = async ({ page, spellIndex }: { page: Page, spellIndex: string }) => {
  await navigateToFavoritePage({ page });
  await waitForSpell({ page, spellIndex });
};
