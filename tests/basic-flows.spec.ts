import { test } from '@playwright/test';
import { navigateToSpellListPage } from './common/navigate';
import { addFirstSpellToFavorite, checkSpellInFavoriteList, waitForFirstSpell } from './steps/spell.steps';

test.describe('Basic flows', () => {
  test('List spells and add the first one to the favorite list', async ({ page, context }) => {
    await navigateToSpellListPage({ page });

    const spellIndex = await addFirstSpellToFavorite({ page });
    await checkSpellInFavoriteList({ page, spellIndex });
  });
});
